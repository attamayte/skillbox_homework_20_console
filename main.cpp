#include <iostream>
#include <string>

enum class Result
{
	SUCCESS			= 0,
	FAILURE			= 1,
	EVAL_ERROR		= 2,
	DISK_DISASTER	= 3,
	// use as end_iter = only for range check
	IMPOSSIBLE
};

Result test(int val)
{
	Result res = static_cast<Result>(val);
	return (res >= Result::SUCCESS && res < Result::IMPOSSIBLE ? res : Result::IMPOSSIBLE);
}

struct GameStudio
{
	enum class Genre { CASH_GRAB, WHAT_MARKET_GUY_SUGGESTS, WE_WANT_SOME_ARTS_AWARD, NO_ONE_WILL_BUY_IT_BUT_WE_DONT_CARE };

	std::string name;
	Genre current_project;
	size_t people_alive;
	double budget_left;
	bool at_crunch;

	GameStudio(const char* _name, Genre curr_proj = Genre::CASH_GRAB, const size_t people = 1, const double budget = 0, const bool crunch = true) 
		: name(_name), current_project(curr_proj), people_alive(people), budget_left(budget), at_crunch(crunch)  {}

};

int main()
{
	for (int work = 0; work < static_cast<int>(Result::IMPOSSIBLE); ++work)
		std::cout << static_cast<int>(test(work)) << '\n';
	
	GameStudio GameExpress("Game Express", GameStudio::Genre::CASH_GRAB, 6, 1'290.0, true);
	GameStudio HeartBreaker("Heart Breaker", GameStudio::Genre::WHAT_MARKET_GUY_SUGGESTS, 49, 11'600'000.0, true);
	GameStudio ChillDudes("Chill Dudes", GameStudio::Genre::NO_ONE_WILL_BUY_IT_BUT_WE_DONT_CARE, 2, 0.0, false);
	
	return 0;
}